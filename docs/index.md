# Olang Process Manager

It is a simple process management tool. You can launch multiple "ola" files and run the background. On your file system, under the pid folder, start a particle for each process and run it in arkapland.

To create a new Opsfile;
```
$ ops init
```

To start the processes;
```
$ ops start
```

To stop the processes;
```
$ ops stop
```

Example Opsfile (taken from [olf](https://github.com/olproject/olf) project);
```
[olang]
boot: boot.ola
```
