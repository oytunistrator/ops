package main

import (
	"os"
	"fmt"
	"flag"
	"log"
	"github.com/olproject/ops/opsfile"
	"io/ioutil"
)

func fatal(args ...interface{}) {
	fmt.Fprintln(os.Stderr, args...)
	os.Exit(1)
}


func make_args(options ...string) string {
	var args string
	for i, opt := range options {
		if len(opt) > 0 {
			args += opt
			if i < len(options)-1 {
				args += " "
			}
		}
	}
	return args
}


func printVersion() {
	fmt.Fprintln(os.Stderr, "Olang Process Manager")
}


func main() {
	flag.Usage = func() {
		printVersion()
		fmt.Fprintln(os.Stderr, "\nUsage of:", os.Args[0])
		fmt.Fprintln(os.Stderr, os.Args[0], " [start|stop|init]")
		flag.PrintDefaults()
	}
	var (
		ver  = flag.Bool("version", false, "Print version number and exit")
	)
	flag.Parse()

	if *ver {
		printVersion()
		os.Exit(1)
	}
	if flag.NArg() != 1 {
		flag.Usage()
		os.Exit(1)
	}

	ty := flag.Arg(0)
	
	wd, _ := os.Getwd()

	if _, err := os.Stat(wd + "/Opsfile"); os.IsNotExist(err) {
		log.Fatal(err)
	}

	runable := opsfile.New("Opsfile")
	
	if ty == "start"{
		runable.Run()
		os.Exit(1)
	}else if ty == "stop"{
		runable.Kill()
		os.Exit(1)
	}else if ty == "init"{
		by := []byte("[olang]\nprocname=filename.ola\n")
		err := ioutil.WriteFile("Opsfile", by, 0777)
		if err != nil{
			log.Fatal(err)
		}
		os.Exit(1)
	}
}